FROM cloudron/base:0.11.0

MAINTAINER Sonatype <cloud-ops@sonatype.com>

LABEL vendor=Sonatype \
      com.sonatype.license="Apache License, Version 2.0" \
      com.sonatype.name="Nexus Repository Manager base image"

ARG NEXUS_VERSION=3.6.0-02
ARG NEXUS_DOWNLOAD_URL=https://download.sonatype.com/nexus/3/nexus-${NEXUS_VERSION}-unix.tar.gz
ARG NEXUS_DOWNLOAD_SHA256_HASH=40b95b097b43cc8941a9700d24baf25ef94867286e43eaffa37cf188726bb2a7

ENV JAVA_HOME=/opt/java \
    JAVA_VERSION_MAJOR=8 \
    JAVA_VERSION_MINOR=152 \
    JAVA_VERSION_BUILD=16 \
    JAVA_DOWNLOAD_HASH=aa0333dd3019491ca4f6ddbe78cdb6d0

ENV JAVA_URL=http://download.oracle.com/otn-pub/java/jdk/${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-b${JAVA_VERSION_BUILD}/${JAVA_DOWNLOAD_HASH}/server-jre-${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-linux-x64.tar.gz \
    JAVA_DOWNLOAD_SHA256_HASH=e8a341ce566f32c3d06f6d0f0eeea9a0f434f538d22af949ae58bc86f2eeaae4

# configure nexus runtime
ENV SONATYPE_DIR=/app/code
ENV NEXUS_HOME=${SONATYPE_DIR}/nexus \
    NEXUS_DATA=/app/data/nexus-data \
    NEXUS_CONTEXT='' \
    SONATYPE_WORK=/app/data/sonatype-work

ARG NEXUS_REPOSITORY_MANAGER_COOKBOOK_VERSION="release-0.3.0-01"
ARG NEXUS_REPOSITORY_MANAGER_COOKBOOK_URL="https://github.com/sonatype/chef-nexus-repository-manager/releases/download/${NEXUS_REPOSITORY_MANAGER_COOKBOOK_VERSION}/chef-nexus-repository-manager.tar.gz"

ADD solo.json.erb /var/chef/solo.json.erb

# Install using chef-solo
RUN curl -L https://www.getchef.com/chef/install.sh | bash \
    && /opt/chef/embedded/bin/erb /var/chef/solo.json.erb > /var/chef/solo.json \
    && chef-solo \
       --recipe-url ${NEXUS_REPOSITORY_MANAGER_COOKBOOK_URL} \
       --json-attributes /var/chef/solo.json \
    && dpkg-query --show --showformat="\${Package} " *chef* | xargs dpkg --purge \
    && rm -rf /etc/chef \
    && rm -rf /opt/chefdk \
    && rm -rf /var/cache/yum \
    && rm -rf /var/chef

RUN sed -e 's,^-XX:LogFile=.*$,-XX:LogFile=/app/data/sonatype-work/nexus3/log/jvm.log,' -i /app/code/nexus/bin/nexus.vmoptions
RUN sed -e 's,^-Dkaraf\.data=.*$,-Dkaraf.data=/app/data/sonatype-work/nexus3,' -i /app/code/nexus/bin/nexus.vmoptions
RUN sed -e 's,^-Djava\.io\.tmpdir=.*$,-Djava.io.tmpdir=/app/data/sonatype-work/nexus3/tmp,' -i /app/code/nexus/bin/nexus.vmoptions
RUN echo "-Djava.util.prefs.userRoot=${NEXUS_DATA}/javaprefs" >> /app/code/nexus/bin/nexus.vmoptions

RUN mv /app/data /app/code/defaultData && mkdir /app/data

ADD start.sh /app/code/start.sh
RUN chmod +x /app/code/start.sh

EXPOSE 8081

ENV INSTALL4J_ADD_VM_PARAMS="-Xms1200m -Xmx1200m -XX:MaxDirectMemorySize=2g -Djava.util.prefs.userRoot=${NEXUS_DATA}/javaprefs"

CMD ["/app/code/start.sh"]
