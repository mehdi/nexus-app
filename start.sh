#!/bin/bash

if [ ! -d /app/data/sonatype-work ] || [ ! -d /app/data/nexus-data ]; then
    cp -r /app/code/defaultData/* /app/data/
fi

chown -R nexus /app/data
exec sudo -u nexus /app/code/start-nexus-repository-manager.sh
